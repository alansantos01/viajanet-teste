import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { AutoCompleteVoyageService } from '../services/auto-complete-voyage.service';

@Component({
  selector: 'app-viaja-net-teste',
  templateUrl: './viaja-net-teste.component.html',
  styleUrls: ['./viaja-net-teste.component.css'],
  providers: [AutoCompleteVoyageService]
})
export class ViajaNetTesteComponent implements OnInit {

  searchTerm : FormControl = new FormControl();
  myVoyage = <any>[];

  constructor (private service: AutoCompleteVoyageService) { }
  
  ngOnInit () {
    this.searchTerm.valueChanges.subscribe(
      term => {
        var termo = term.length;
        console.log(term);
        if (termo > 2) {
          this.service.search(term).subscribe(
            data => {
              this.myVoyage = data as any[];
              //console.log(data[0].Name);
          })
        }
    })
  }
}
