import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViajaNetTesteComponent } from './viaja-net-teste.component';

describe('ViajaNetTesteComponent', () => {
  let component: ViajaNetTesteComponent;
  let fixture: ComponentFixture<ViajaNetTesteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViajaNetTesteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViajaNetTesteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
