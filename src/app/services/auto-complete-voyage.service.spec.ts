import { TestBed } from '@angular/core/testing';

import { AutoCompleteVoyageService } from './auto-complete-voyage.service';

describe('AutoCompleteVoyageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AutoCompleteVoyageService = TestBed.get(AutoCompleteVoyageService);
    expect(service).toBeTruthy();
  });
});
