import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { debounceTime } from 'rxjs/internal/operators/debounceTime';

@Injectable({
  providedIn: 'root'
})
export class AutoCompleteVoyageService {

  constructor (private httpService: HttpClient) { }  

  search(term) {
      var listOfCitys = this.httpService.get('https://www.viajanet.com.br/resources/api/Autocomplete/' + term)
      .pipe(
          debounceTime(500),  // WAIT FOR 500 MILISECONDS ATER EACH KEY STROKE.
          map(
              (data: any) => {
                
                  return (
                      data != '' ? data as any[] : [{"Name": "Nenhum local encontrado!"} as any]
                  );
              }
      ));
      return listOfCitys;
  }  
}
